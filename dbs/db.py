'''
Demonstrate	in	Python	how	you	would connect to a MySQL database that
uses the InnoDB	storage	engine and query for all records (with the 
following fields: name,	age, role) from	a table	called "dedsert_test", also	
provide	an example of how you would	write a	record	to the same table.
'''
import random,names
import MySQLdb

'''
List of roles.
'''
ROLES = [
	'Technical specialist', 
	'Software developer', 
	'Accountant', 
	'Human resources staff',
	'Cook',
	'Support',
	'Data analyst',
	'Strategist',
	'Project manager'
]

'''
A database db wrapper.
'''
class DatabaseWrapper(object):
	def __init__(self, host, user, passwd, db):
		try:
			self.conn = MySQLdb.connect(host=host, user=user,passwd=passwd,db=db)
		except:
			self.conn = None

	def get_cursor(self):
		try:
			return self.conn.cursor()
		except:
			return None

	def get_conn(self):
		return self.conn

'''
This function creates a table if it's not already created.
'''

def create_table(db_wrapper):
	cursor = db_wrapper.get_cursor()
	try:
		cursor.execute("CREATE TABLE IF NOT EXISTS dedsert_test ( id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT, name TEXT NOT NULL, age INT UNSIGNED, role text )")
		#db_wrapper.conn.commit()
	except MySQLdb.Warning:
		pass
	cursor.close()

'''
This function inserts random data.
'''

def random_inserts(db_wrapper, n_inserts, names):
	cursor = db_wrapper.get_cursor()
	for i in range(n_inserts):
		name=names[random.randint(0,len(names)-1)]
		age = str(random.randint(18,45))
		role = ROLES[random.randint(0,len(ROLES)-1)]
		try:
			cursor.execute("INSERT INTO dedsert_test(name,age,role) VALUES (%s, %s, %s)", (name, age, role))
			db_wrapper.get_conn().commit()
		except:
			db_wrapper.get_conn().rollback()
	cursor.close()

'''
Function that queries all the data, selecting only desired fields and 
returns results as a list of tuples.
'''

def select_all(db_wrapper):
	cursor = db_wrapper.get_cursor()
	try:
		cursor.execute("SELECT name,age,role FROM dedsert_test")
		l = [row for row in cursor.fetchall()]
		cursor.close()
		return l
	except:
		return None

'''
Drop table
'''
def drop_table(db_wrapper):
	cursor = db_wrapper.get_cursor()
	try:
		cursor.execute("DROP TABLE dedsert_test")
		cursor.close()
	except:
		return None

