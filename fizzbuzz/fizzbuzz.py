'''
Write a	program	that prints	the	numbers	from 1 to 100. But for multiples
of thee	print “Fizz” instead of	the	number and for multiples of	five print	
“Buzz”.	For	numbers	that are multiples of both three and five print “FizzBuzz”.
'''

def fizz_buzz():
	base = [x for x in range(1,101)]
	mult_of_three = filter(lambda x: x % 3 == 0, base)
	mult_of_five = filter(lambda x: x % 5 == 0, base)
	for n in mult_of_three:
		base[n-1]="Fizz"
	for n in mult_of_five:
		if base[n-1] == "Fizz":
			base[n-1]="FizzBuzz"
		else:
			base[n-1]="Buzz"
	return base

<<<<<<< HEAD:fizzbuzz/fizzbuzz.py
=======
def main():
	print fizz_buzz()

if __name__ == '__main__':
    main()
>>>>>>> 5e64eb32ca47461bd07e34d5a8928078e604f5f0:fizzbuzz/main.py
