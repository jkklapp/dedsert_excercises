import unittest
from fizz_buzz import fizz_buzz

class TestFizzBuzz(unittest.TestCase):

    def setUp(self):
        self.base = [x for x in range(1,101)]
        self.result = fizz_buzz()
        self.mult_of_three = filter(lambda x: x % 3 == 0, self.base)
        self.mult_of_five = filter(lambda x: x % 5 == 0, self.base)

    def test_find_all_fizzes(self):
        data = filter(lambda x: x not in self.mult_of_five, self.mult_of_three)
        map(lambda x: self.assertEqual(self.result[x-1], "Fizz"), data)

    def test_find_all_buzzes(self):
        data = filter(lambda x: x not in self.mult_of_three, self.mult_of_five)
        map(lambda x: self.assertEqual(self.result[x-1], "Buzz"), data)

    def test_find_all_fizzbuzzes(self):
        data = filter(lambda x: x in self.mult_of_five, self.mult_of_three)
        map(lambda x: self.assertEqual(self.result[x-1], "FizzBuzz"), data)

if __name__ == '__main__':
    unittest.main()