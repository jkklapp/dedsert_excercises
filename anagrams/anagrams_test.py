import unittest
from anagrams import find_anagrams

class TestAnagrams(unittest.TestCase):

    def test_find(self):
        r = find_anagrams('army', ['mary', 'ramyy', 'terry', 'yarm'])
        self.assertEqual(r, ['mary', 'yarm'])

if __name__ == '__main__':
    unittest.main()