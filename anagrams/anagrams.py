'''Find the anagram
Write a function that accepts a word (string) and a list of words (list or 
tuple of strings) and return back a list with the valid anagrams for the 
word inside the given words list.'''

def find_anagrams(word, candidates):
	r = []
	word = list(word)
	for candidate in candidates:
		c = list(candidate)
		if len(c) != len(word):
			continue
		test = [x for x in word if x not in c]
		if len(test) == 0:
			r.append(candidate)
	return r

