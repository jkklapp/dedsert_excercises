import unittest
from get_numbers import get_numbers

class TestGetNumbers(unittest.TestCase):
	def testNormal(self):
		list1 = [1, 2, 4, 5, 65, 76, 23, 54, 65, 34, 1, 2]
		list2 = [2, 1, 5, 4, 76, 65, 23, 65, 54, 34, 1, 2]
		self.assertEqual(get_numbers(list1,list2), [34, 4, 5, 76, 54, 23])
        
	def testEmpty(self):
		list1 = []
		list2 = []
		self.assertEqual(get_numbers(list1,list2), [])
        
	def testGetEmpty(self):
		list1 = [1, 2, 1, 2]
		list2 = [2, 1, 2, 1]
		self.assertEqual(get_numbers(list1,list2), [])

	def testDifferentLengths(self):
		list1 = [1, 2, 3, 4, 5]
		list2 = [6, 7, 8]
		self.assertEqual(get_numbers(list1,list2), [])

	def testNoCommonElements(self):
		list1 = [1, 2, 3, 4, 5]
		list2 = [6, 7, 8, 9, 10]
		self.assertEqual(get_numbers(list1,list2), [])

if __name__ == '__main__':
	unittest.main()


