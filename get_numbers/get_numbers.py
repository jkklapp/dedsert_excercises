'''
	Divide and conquer:
		for each n in L1:
			check if n appears only once in L2
			in O(n) time.

	We start at i = 0 until the length of L2)
		* check if L2[i] is n
			* if it is, 
				* We remove the element from L2,
				* Take note of it
					* If already taken note of,
					  we don't need it anymore.
			* i = i + 1

	After this, we have a list of n's appearing 
	only once in L2.

	Time complexity study:

	Let |L1| ~ |L2| as N:

		O(N) * O(N) = O(N^2)

'''

def get_numbers(L1, L2):
	if not L1 or not L2:
		return []
	r = {}
	for n in L1: # This block is O(|L1|)
		i = 0
		while i <= len(L2)-1: # This block is O(|L2|)
			if n == L2[i]:
				L2.pop(i)
				r[n] = 1 if n not in r else None
				if not r[n]:
					del r[n]
			i += 1
	return r.keys()
