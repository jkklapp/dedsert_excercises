'''
Least recently used cache: Implement a least recently used (LRU) 
cache mechanism using a decorator and demonstrate it use in a 
small script. The LRU must be able to admit a 'max_size' parameter
that by default has to be 100.
'''

from db import DatabaseWrapper
from db import random_inserts
from decorator import decorator
import names, random


class cache(object):
	def __init__(self, max):
		self.max = max

	def __call__(self, func):
		return decorator(self.check_cache, func)

	def check_cache(self, func, db, name):	
		if not hasattr(func, 'results'):
			func.results = {}
			func.keys = []
		if name not in func.results:
			if len(func.results) == self.max:
				func.keys.pop(len(func.keys)-1)
				func.keys = [name] + func.keys
			else:
				func.keys.append(name)
			data = func(db, name)
			if len(data) != 0:
				func.results[name] = data
				func.keys.pop(func.keys.index(name))
				func.keys = [name] + func.keys
			else:
				func.keys.pop(func.keys.index(name))
				return None
		return func.results[name]

@cache(max=100)
def query_name_age_role(db, name):
	cursor = db.get_cursor()
	try:
		cursor.execute("SELECT name,age,role FROM dedsert_test where name = \'"+name+"\'")
		return [row for row in cursor.fetchall()]
	except:
		return None


def main():
	test_names = [str(names.get_full_name()) for i in range(1000000)]
	print "Looking for ", test_names, " in the DB."
	db_wrapper = DatabaseWrapper("localhost", "dedsert", "1234", "dedsert")
	#random_inserts(db_wrapper,100000)
	for name in test_names:
		q = query_name_age_role(db_wrapper, name)
		if q: print q
	db_wrapper.get_conn().close()
	print "Looking again with no DB."
	try:
		for name in test_names:
			q = query_name_age_role(db_wrapper, name)
			if q: print q
	except:
		pass
	#print query_name_age_role.keys
	#print query_name_age_role.results


if __name__ == '__main__':
	main()