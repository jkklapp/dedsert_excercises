import unittest
from db import DatabaseWrapper
from db import create_table, random_inserts, select_all
from main import query_name_age_role

class LRUCacheTest(unittest.TestCase):

    def setUp(self):
        self.db_wrapper = DatabaseWrapper("localhost", "dedsert", "1234", "dedsert")

    def test_connection(self):
        self.assertIsNot(self.db_wrapper.conn, None)

    def test_get_cursos(self):
        self.assertIsNot(self.db_wrapper.get_cursor(), None)

    def test_insert_and_select(self):
        create_table(self.db_wrapper)
        random_inserts(self.db_wrapper,1)
        r1 = select_all(self.db_wrapper)
        random_inserts(self.db_wrapper,1)
        r2 = select_all(self.db_wrapper)
        self.assertIsNot(r1, None)
        self.assertIsNot(r2, None)
        self.assertNotEqual(r1, r2)

if __name__ == '__main__':
    unittest.main()