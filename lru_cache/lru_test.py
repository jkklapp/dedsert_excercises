import unittest
from db import DatabaseWrapper
from db import create_table, drop_table, random_inserts, select_all
from lru import query_name
import names

class LRUCacheTest(unittest.TestCase):

    def setUp(self):
        self.db_wrapper = DatabaseWrapper("localhost", "dedsert", "1234", "dedsert")
        create_table(self.db_wrapper)
        self.test_names = [str(names.get_full_name()) for i in range(10)]
        random_inserts(self.db_wrapper, 1000, self.test_names)

    def tearDown(self):
        drop_table(self.db_wrapper)
        #self.db_wrapper.get_conn().close()

    def testQueryName(self):
        for name in self.test_names:
            q = query_name(self.db_wrapper, name)
        self.db_wrapper.conn.close()
        for name in self.test_names:
            q = query_name(self.db_wrapper, name)
            self.assertTrue(q)

if __name__ == '__main__':
    unittest.main()