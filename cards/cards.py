'''
Cards Game
Write a console application that simulates of a card game that mix up a 
standard deck of cards and then deal the cards to the players without 
using the same card more than once.
'''
import itertools
import random
import names
import sys

class Deck:
    """ Deck class. Has suits and values. When you get a card, the 
    card is removed from the Deck."""
    def __init__(self):
        self.suits = ['Spades', 'Hearts', 'Diamonds', 'Clubs']
        self.values = range(1,14)
        self.cards = [] #Empty List to Append
        for card in itertools.product(self.suits,self.values):
            self.cards.append(card) #Cartesian Product to Create Deck'''
    def GetRandomCard(self):
    	index = random.randint(0,len(self.cards)-1)
        card = self.cards[index] #Generates Random Card
        self.cards.pop(index)
    	return card

class Player:
	""" Player class. Has a name, an ID and a Card. """
	def __init__(self,card):
		self.name = names.get_first_name()+" "+names.get_last_name()
		self.card = card

	def getCardValue(self):
		if self.card[1] == 1:
			return "A"
		elif self.card[1] == 11:
			return "J"
		elif self.card[1] == 12:
			return "Q"
		elif self.card[1] == 13:
			return "K"
		else:
			return str(self.card[1])

	def getCardSuit(self):
		return str(self.card[0])
	def printPlayer(self, winner=False):
		if winner:
			print "Player "+self.name+" has card: "+self.getCardValue()+" of "+self.getCardSuit()+" (WINNER)"
		else:
			print "Player "+self.name+" has card: "+self.getCardValue()+" of "+self.getCardSuit()

class Game(object):
    def __init__(self,n_players):
        self.n_players = n_players
        self.players = []
    def StartGame(self):
        cards = Deck()
        self.players = [Player(cards.GetRandomCard()) for x in range(self.n_players)]
        self.DecideWinner()

    def DecideWinner(self):
        winner = self.players[0] #Choose Player 0 as Potential Winner
        for player in self.players:
            if(player.card[1] > winner.card[1]):
                winner = player #Find the Player with Highest Card
        for player in self.players:
        	if player == winner:
    			player.printPlayer(True)
    		else:
				player.printPlayer()

if __name__=="__main__":
	if len(sys.argv) != 2:
		print "python ./cards <number of players>"
		sys.exit()
	if int(sys.argv[1]) > 52:
		print "52 players MAX!"
		sys.exit()
	game = Game(int(sys.argv[1]))
	game.StartGame()