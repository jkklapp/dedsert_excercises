'''
Date Calculation
The	Irish lottery draw takes place twice weekly on a Wednesday and a 
Saturday at 8pm. Write a function that calculates and returns the 
next valid draw date based on the current date and time and also on 
an optional supplied date.
'''

import datetime
import sys
import time

days = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']

def get_today_weekday():
	return now().weekday()

def get_today_weekday_text():
	return days[get_today_weekday()]

def now():
	return datetime.datetime.today()

def get_weekday_from_date(date):
	return date.strftime('%a')

def get_next_valid_lottery_date(date, f):
	try:
		t = datetime.datetime.strptime(date, f)
	except ValueError:
		print "Please stick to the format" + f + " or provide one."
		sys.exit()
	day = get_weekday_from_date(t)
	i = days.index(day)
	delay = 0
	while days[i+delay] not in ['Wed', 'Sun']:
	 	delay += 1
	end_date = t + datetime.timedelta(days=delay)
	return end_date

def get_next_valid_lottery_date_with_format(date, f='%Y-%m-%d'):
	return get_next_valid_lottery_date(date, f).strftime(f)

def get_very_next_lottery_date(f='%Y-%m-%d'):
	return get_next_valid_lottery_date_with_format(str(now()).split()[0], f)

def main():
	if len(sys.argv) > 3:
		print "usage: python date_calc [date_to_calc_next] [format]"
		print "Default date format: '%Y-%m-%d'"
		sys.exit()

	input_format = None
	if len(sys.argv) == 3:
		input_format = sys.argv[2]
	input_date = str(now()).split()[0]
	if len(sys.argv) >= 2:
		input_date = sys.argv[1]
		
	print get_next_valid_lottery_date_with_format(input_date, input_format)


if __name__ == '__main__':
	main()
