import unittest
import datetime
from date_calc import get_next_valid_lottery_date_with_format
from date_calc import get_very_next_lottery_date
from date_calc import get_today_weekday_text
from date_calc import days

class TestDateCalc(unittest.TestCase):

    def test_get_next_valid_date(self):
        today = get_today_weekday_text()
        today_index = days.index(today)
        next_date = get_very_next_lottery_date()
        if today_index < days.index('Wed'):
            self.assertEqual(datetime.datetime.strptime(next_date, '%Y-%m-%d').weekday(), 'Wed')
        else:
            self.assertEqual(days[datetime.datetime.strptime(next_date, '%Y-%m-%d').weekday()], 'Sun')

    def test_get_a_wednesday(self):
        date = '2014-05-12'
        next_date = get_next_valid_lottery_date_with_format(date)
        self.assertEqual(next_date, '2014-05-14')

    def test_get_next_with_month_change(self):
        # This day is Tuesday.
        date = '06/30/2015'
        # The next valid should be July the first. 
        next_date = get_next_valid_lottery_date_with_format(date, '%m/%d/%Y')
        self.assertEqual(next_date, '07/01/2015')

    def test_another_format(self):
        date = 'Jun 30, 2015'
        next_date = get_next_valid_lottery_date_with_format(date, '%b %d, %Y')
        self.assertEqual(next_date, 'Jul 01, 2015')

if __name__ == '__main__':
    unittest.main()